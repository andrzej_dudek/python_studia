import numpy as np
import random

def generate_random_array_2d(dimension1, dimension2):
    arr = np.ones((dimension1, dimension2))
    for i in range(dimension1):
        for j in range(dimension2):
            arr[i][j] = random.random()
    return arr

dim = int(input("To calculate det(A) of random NxN matrix insert N: "))
A = generate_random_array_2d(dim,dim)
print(A)
detA = np.linalg.det(A)
print("det(A) = {}".format(detA))

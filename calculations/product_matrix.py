import numpy as np
import random

def generate_random_array_2d(dimension1, dimension2):
    arr = np.ones((dimension1, dimension2))
    for i in range(dimension1):
        for j in range(dimension2):
            arr[i][j] = random.randrange(10)
    return arr

def product_matrix(arr1, arr2):
    dim = arr1.shape[0]
    arr = np.zeros((dim,dim))
    for r in range(dim):
        for i in range(dim):
            for j in range(dim):
                arr[i][j] += arr1[i][r]*arr2[r][j]
    return arr


A = generate_random_array_2d(8,8)
B = generate_random_array_2d(8,8)
C = product_matrix(A,B)

print(A)
print("*")
print(B)
print("=")
print(C)

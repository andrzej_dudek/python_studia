import sys
import math

a = float(sys.argv[1])
b = float(sys.argv[2])
c = float(sys.argv[3])

# y = ax^2 + bx + c
# only for real (not for complex values)

delta = (b ** 2) - (4 * a * c)

if delta > 0:
    x1 = (-b - math.sqrt(delta)) / (2*a)
    x2 = (-b + math.sqrt(delta)) / (2*a)
    print("This quadratic equation have roots:")
    print("x1 = {}\nx2 = {}".format(x1,x2))
elif delta == 0:
    x0 = -b / (2*a)
    print("This quadratic equation has one root:")
    print("x0 = {}".format(x0))
else:
    print("This quadratic equation does not have real roots.")

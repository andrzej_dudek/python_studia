a = [1,2,12,4]
b = [2,4,2,8]

result = 0

for i, j in zip(a,b):
    result += i*j

print("Result of scalar porduct of vectors {} and {} is {}".format(a,b,result))

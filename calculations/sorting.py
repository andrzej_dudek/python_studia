import random

def sorting(array):
    if len(array) > 1:
        mid = int(len(array)/2)
        l_half = array[:mid]
        r_half = array[mid:]

        l_half = sorting(l_half)
        r_half = sorting(r_half)

        array = []

        while len(l_half) > 0 and len(r_half) > 0:
            if l_half[0] > r_half[0]:
                array.append(l_half[0])
                l_half.pop(0)
            else:
                array.append(r_half[0])
                r_half.pop(0)

        for num in l_half:
            array.append(num)
        for num in r_half:
            array.append(num)

    return array


numbers = list()
for element in range(50):
    numbers.append(random.random())

print(numbers)
print("\nNumbers are being sorted.\n")
numbers = sorting(numbers)
print(numbers)

numbers1 = numbers
numbers1.sort(reverse=True)
flag = 0

for n in range(len(numbers)):
    if numbers[n] != numbers1[n]:
        flag = 1

if flag == 0:
    print("\nReference method output is the same as implemented method.")
else:
    print("\nSomething is not working.")

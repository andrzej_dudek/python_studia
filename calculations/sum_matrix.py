import numpy as np
import random

def generate_random_array_2d(dimension1, dimension2):
    arr = np.ones((dimension1, dimension2))
    for i in range(dimension1):
        for j in range(dimension2):
            arr[i][j] = random.randrange(10)
    return arr

def add_arrays(arr1, arr2):
    dim1,dim2 = arr1.shape
    arr = np.zeros((dim1,dim2))
    for i in range(dim1):
        for j in range(dim2):
            arr[i][j] = arr1[i][j] + arr2[i][j]
    return arr

A = generate_random_array_2d(128,128)
B = generate_random_array_2d(128,128)
C = add_arrays(A,B)

print(A)
print("+")
print(B)
print("=")
print(C)

from complex import Complex

def help():
    print("Calcuator accepts mathematical operations in format:")
    print("a +/- bi +/-/*/: c +/- di")
    print("+ - addition\n- - subtraction\n* - multiplication\n: - division")
    print("During multiplication and division z1 = a +/- bi and\nz2 = c +/- di are arguments of the operator.")

def exiting():
    exit()

print("Calcuator for Complex numbers")
print("\nFor help type help.\nTo exit type exit\n")

while True:
    equation = input("Type here: ")

    if "help" in equation:
        help()
        continue

    if "exit" in equation:
        exiting()

    if len(equation.split()) != 7:
        print("Something is incorect. Try to use help")
    else:
        equation = equation.split()
        operation = equation[3]
        a = float(equation[0])
        b = float(equation[1] + equation[2][:-1])
        c = float(equation[4])
        d = float(equation[5] + equation[6][:-1])
        z1 = Complex(a,b)
        z2 = Complex(c,d)
        if operation == '+':
            Complex.sum(z1,z2).show_value()
        elif operation == '-':
            Complex.difference(z1,z2.conjugate()).show_value()
        elif operation == '*':
            Complex.product(z1,z2).show_value()
        elif operation == ':':
            Complex.division(z1,z2).show_value()
        else:
            print("Invalid operation, use help.")

class Complex:
    def __init__(self,real,imag):
        self.r = real
        self.i = imag

    def show_value(self):
        if self.i < 0:
            print("{} - {}i".format(self.r, abs(self.i)))
        else:
            print("{} + {}i".format(self.r, self.i))

    def conjugate(self):
        return(Complex(self.r, -self.i))

    def abs(self):
        return(self.r ** 2 + self.i ** 2)

    def sum(a,b):
        real = a.r + b.r
        imag = a.i + b.i
        return Complex(real,imag)

    def difference(a,b):
        real = a.r - b.r
        imag = a.i - b.i
        return Complex(real,imag)

    def product(a,b):
        real = a.r * b.r + (-1 * (a.i * b.i))
        imag = a.r * b.i + a.i * b.r
        return Complex(real, imag)

    def division(a,b):
        c = Complex.product(a, b.conjugate())
        div = b.abs()
        try:
            real = c.r/div
            imag = c.i/div
            d = Complex(real,imag)
        except ZeroDivisionError:
            print("Do not divide by 0! Now \"0 + 0i\" will be displayed.")
            d = Complex(0,0)
        finally:
            return d

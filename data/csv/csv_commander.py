#id, imie, nazwisko, dział

import csv

filepath = "sample.csv"

def read_from_file(filepath):
    with open(filepath, newline='') as file:
        content = csv.reader(file)
        content = list(content)
    return content

def print_file_content(content):
    for item in content:
        print("-----Person-----")
        print("ID: {}".format(item[0]))
        print("Name: {}".format(item[1]))
        print("Surname: {}".format(item[2]))
        print("Department: {}".format(item[3]))

def help():
    print("\nType: \nadd - to add new record")
    print("del - to delete a record")
    print("help - to get help")
    print("print - to print current content")
    print("exit - to save and exit from program.\n")

def add(content):
    if len(content) == 0 :
        id = "1"
    else:
        id = str(int(content[-1][0]) + 1)
    name = input("Name: ")
    surname = input("Surname: ")
    department = input("Department: ")
    content.append([id,name,surname,department])
    return content

def delete(content):
    id = input("Type the id of the record you want to delete: ")
    flag = 0
    for n in range(len(content)):
        if content[n][0] == id:
            content.pop(n)
            flag = 1
    if flag == 0:
        print("The list does not contain record with id = {}.".format(id))
    else:
        print("The removal was succesful.")
    return content

def save_and_exit(content):
    with open(filepath, 'w', newline='') as file:
        filewriter = csv.writer(file)
        filewriter.writerows(content)
        #for item in content:
        #    filewriter.writerow(item)
    exit()

content = read_from_file(filepath)
print_file_content(content)
help()

while True:
    action = input("Type action you want to take: ")

    if "help" in action:
        help()
    elif "add" in action:
        content = add(content)
    elif "del" in action:
        content = delete(content)
    elif "exit" in action:
        save_and_exit(content)
    elif "print" in action:
        print_file_content(content)
    else:
        print("There is not such command.")

from xml.dom import minidom

domtree = minidom.parse("food.xml")
menu = domtree.documentElement

food = menu.getElementsByTagName('food')

for dish in food:
    print("-----DISH-----")
    if dish.hasAttribute('id'):
         print("ID: {}".format(dish.getAttribute('id')))

    print("Name: {}".format(dish.getElementsByTagName('name')[0].childNodes[0].data))
    print("Price: {}".format(dish.getElementsByTagName('price')[0].childNodes[0].data))
    print("Description: {}".format(dish.getElementsByTagName('description')[0].childNodes[0].data))
    print("Calories: {}".format(dish.getElementsByTagName('calories')[0].childNodes[0].data))

food[1].getElementsByTagName('price')[0].childNodes[0].nodeValue = "$8.95"
domtree.writexml(open('food2.xml', 'w'))

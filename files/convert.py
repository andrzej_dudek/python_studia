from PIL import Image
import os

path_to_photos = "photos/"
all_files = os.listdir(path_to_photos)
jpg_files = [file for file in all_files if file[-4:] == ".jpg"]

for photo in jpg_files:
    photo_name_to_save = photo[:-4] + ".png"
    im = Image.open(path_to_photos+photo)
    im.save(path_to_photos+photo_name_to_save, "PNG")

import random
import os


default_path = "/"

def print_files_in_dir(path_to_dir):
    if path_to_dir[-1] != "/":
        path_to_dir += "/"
    all_files = os.listdir(path_to_dir)
    directories = [dir for dir in all_files if os.path.isdir(path_to_dir + dir)]

    print("\nIn directory \"{}\" there are files:\n".format(path_to_dir))
    for file in directories:
        print(file)

    if len(directories) == 0:
        pass
    else:
        new_dir = random.randrange(0,len(directories))
        path_to_dir += (directories[new_dir] + "/")
        print_files_in_dir(path_to_dir)


print_files_in_dir(default_path)

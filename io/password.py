import os

filename = "pass.txt"

if os.path.isfile(filename) == False:
    f = open(filename, "w")
    f.close()

with open(filename) as file:
    password = file.read()

if password == '':
    password = input("Please set the password:")
    with open(filename, "w") as file:
        file.write(password)
else:
    user_input = input("To access the system please provide the password:")
    if user_input == password:
        print("Access granted!")
    else:
        print("Access denied!")

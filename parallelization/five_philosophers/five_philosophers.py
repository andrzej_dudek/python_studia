import threading
import time
import random

class Philosopher(threading.Thread):

    running_flag = True

    def __init__(self, name, left_fork, right_fork):
        threading.Thread.__init__(self)
        self.name = name
        self.left_fork = left_fork
        self.right_fork = right_fork

    def run(self):
        while self.running_flag:
            time.sleep(random.uniform(2,12))
            print("{} is hungry.".format(self.name))
            self.try_to_eat()

    def try_to_eat(self):
        fork_l = self.left_fork
        fork_r = self.right_fork

        while self.running_flag:
            fork_l.acquire(True)
            locked = fork_r.acquire(False)
            if locked:
                break
            fork_l.release()
            print("{} swaps forks.".format(self.name))
            fork_l, fork_r = fork_r, fork_l
        else:
            return

        self.eat()
        fork_l.release()
        fork_r.release()

    def eat(self):
        print("{} starts eating.".format(self.name))
        time.sleep(random.uniform(1,6))
        print("{} finished eating and now starts thinking.".format(self.name))

forks = [threading.Lock() for n in range(5)]
philosophers_names = ["Aristotle", "Kant", "Augustin",
                      "Thomas More", "Machiavelli"]
philosophers = [Philosopher(philosophers_names[n], forks[n%5], forks[(n+1)%5]) \
               for n in range(5)]

random.seed(3)
Philosopher.running_flag = True

for philosopher in philosophers:
    philosopher.start()
time.sleep(60)
Philosopher.running_flag = False

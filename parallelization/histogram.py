import matplotlib.pyplot as plt
import numpy as np

data = np.random.normal(300, 10, (100000))
data.sort()

maximum = data[-1]
minimum = data[0]
num_of_comp = int(np.sqrt(data.size))
step = (maximum - minimum)/num_of_comp
steps = np.arange(minimum,maximum,step)
histogram = dict.fromkeys(steps,0)

n = data.size - 1
k = steps.size - 1
while (n >= 0 and k >= 0):
    if data[n] >= steps[k]:
        histogram[steps[k]] += 1
        n -= 1
    else:
        k -= 1

hist1 = plt.subplot()
hist1.bar(histogram.keys(),histogram.values())
plt.show()

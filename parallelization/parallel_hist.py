import numpy as np
import fast_histogram
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import matplotlib.pyplot as plt

loc = 300
scale = 10

np.random.seed(1)
# histogram will be 1d, but data in 2d array
vals = np.random.normal(loc=300,scale=10,size=[2, 10000000]).astype(np.float32)

print(vals.shape)
print(np.sqrt(vals.shape[1]))
print(vals[:,1])

splits = 6
with ThreadPoolExecutor(max_workers=splits) as pool:
    chunk = vals.shape[1] // splits
    chunk0 = [vals[0,i*chunk:(i+1)*chunk] for i in range(splits)]
    chunk1 = [vals[1,i*chunk:(i+1)*chunk] for i in range(splits)]
    f = partial(fast_histogram.histogram2d, bins=255, range=((loc-scale,loc+scale),(loc-scale,loc+scale)))
    results = pool.map(f, chunk0, chunk1)
    results = sum(results)

print(results[1,:])
max = np.max(vals)
min = np.min(vals)
x = np.linspace(min,max,255)
y = results[1,:]+results[2,:]

hist1 = plt.subplot()
hist1.bar(x,y)
plt.show()

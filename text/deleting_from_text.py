filepath = 'filepath'
filepath_to_write = 'filepath2write'

words_to_delete = (" się ", " i ", " nigdy ", " dlaczego ")

def delete_from_text(text, words_to_delete):
    for word in words_to_delete:
        text.replace(word, " ")
    return text

if filepath == 'filepath':
    print("You need to change filepath to file you want to read.")
    exit()

with open(filepath, 'r') as file:
    content = file.read()

content = delete_from_text(content, words_to_delete)

if filepath_to_write == 'filepath2write':
    print("You need to change filepath to file you want to save.")
    exit()

with open(filepath_to_write, 'w') as file:
    file.write(content)

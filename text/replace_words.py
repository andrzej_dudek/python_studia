filepath = 'filepath'
filepath_to_write = 'filepath2write'

word_replace_repeat = [" i ", " oraz "]
word_replace_dict = {" nigdy ": " prawie nigdy", " dlaczego ": " czemu "}

def replace_repeat(text, repeating_words):
    text = text.split(repeating_words[0])
    for n in range(len(text)):
        temp_item = text[n].split(repeating_words[1])
        if len(temp_item) == 1:
            continue
        else:
            text[n] = repeating_words[0].join(temp_item)
    text = repeating_words[1].join(text)
    return text

def normal_replace(text, replace_dict):
    for key in replace_dict.keys():
        text.replace(key, replace_dict[key])
    return text

 if filepath == 'filepath':
     print("You need to change filepath to file you want to read.")
     exit()

 with open(filepath, 'r') as file:
     content = file.read()

 content = replace_repeat(content, word_replace_repeat)
 content = normal_replace(content, word_replace_dict)

 if filepath_to_write == 'filepath2write':
     print("You need to change filepath to file you want to save.")
     exit()

 with open(filepath_to_write, 'w') as file:
     file.write(content)
